package impl

import (
	"database/sql"

	"gitee.com/lbzs/demo/hostdemo/api/conf"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	Service = &service{}
)

type service struct {
	l  logger.Logger
	db *sql.DB
}

func (s *service) Config() error {
	db, err := conf.C().MySQL.GetDB()
	if err != nil {
		return err
	}

	s.l = zap.L().Named("Host")
	s.db = db
	// s.l = zap.L().Named("Policy")
	return nil
}

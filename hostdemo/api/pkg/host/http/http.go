package http

import (
	"fmt"

	"gitee.com/lbzs/demo/hostdemo/api/pkg"
	"gitee.com/lbzs/demo/hostdemo/api/pkg/host"

	// "github.com/infraboard/mcube/http/router/httprouter"
	// "github.com/infraboard/mcube/http/router/httprouter"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/julienschmidt/httprouter"
)

type handler struct {
	service host.Service
	log     logger.Logger
}

var (
	api = &handler{}
)

func (h *handler) Config() error {
	h.log = zap.L().Named("Host")
	if pkg.Host == nil {
		return fmt.Errorf("dependence service host not ready")
	}
	h.service = pkg.Host
	return nil
}

func RegistAPI(r *httprouter.Router) {
	api.Config()
	// r.Get("/hosts", api.QueryHost)
	r.GET("/hosts", api.QueryHost)
	r.POST("/hosts", api.CreateHost)
}

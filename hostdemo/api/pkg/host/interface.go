package host

import (
	"context"
	"net/http"
	"strconv"
)

type Service interface {
	SaveHost(context.Context, *Host) (*Host, error)
	QueryHost(context.Context, *QueryHostRequest) (*HostSet, error)
}

type QueryHostRequest struct {
	PageSize   uint64 `json:"page_size, omitempty"`
	PageNumber uint64 `json:"page_number, omitempty"`
}

func (req *QueryHostRequest) Offset() int64 {
	return int64(req.PageSize) * int64(req.PageNumber-1)
}

func NewQueryHostRequest(r *http.Request) *QueryHostRequest {
	qs := r.URL.Query()
	ps := qs.Get("page_size")
	pn := qs.Get("page_number")
	psUint64, _ := strconv.ParseUint(ps, 10, 64)
	pnUint64, _ := strconv.ParseUint(pn, 10, 64)

	if psUint64 == 0 {
		psUint64 = 20
	}
	if pnUint64 == 0 {
		pnUint64 = 1
	}
	return &QueryHostRequest{
		PageSize:   psUint64,
		PageNumber: pnUint64,
		// Keywords:   kw,
	}
	// return &QueryHostRequest{}
}

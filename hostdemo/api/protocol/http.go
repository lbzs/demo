package protocol

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitee.com/lbzs/demo/hostdemo/api/conf"
	// "github.com/infraboard/mcube/http/router/httprouter"
	// "github.com/infraboard/mcube/http/middleware/cors"
	hostAPI "gitee.com/lbzs/demo/hostdemo/api/pkg/host/http"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
)

type HTTPService struct {
	r      *httprouter.Router
	l      logger.Logger
	c      *conf.Config
	server *http.Server
}

func NewHTTPService() *HTTPService {
	r := httprouter.New()
	server := &http.Server{
		ReadHeaderTimeout: 60 * time.Second,
		ReadTimeout:       60 * time.Second,
		WriteTimeout:      60 * time.Second,
		IdleTimeout:       60 * time.Second,
		MaxHeaderBytes:    1 << 20,
		Addr:              conf.C().App.Addr(),
		Handler:           cors.AllowAll().Handler(r),
	}
	return &HTTPService{
		r:      r,
		server: server,
		l:      zap.L().Named("API"),
		c:      conf.C(),
	}
}

func (s *HTTPService) Start() error {
	hostAPI.RegistAPI(s.r)
	s.l.Infof("Http服务启动成功，监听地址: %s\n", s.server.Addr)
	if err := s.server.ListenAndServe(); err != nil {
		if err == http.ErrServerClosed {
			s.l.Info("Service is stopped")
		}
		return fmt.Errorf("start service error, %s", err.Error())
	}
	return nil
}

func (s *HTTPService) Stop() error {
	s.l.Info("start graceful shutdown")
	ctx, cancle := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancle()
	if err := s.server.Shutdown(ctx); err != nil {
		s.l.Errorf("graceful shutdown timeout, force exit")
	}
	return nil
}

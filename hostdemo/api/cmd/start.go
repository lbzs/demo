package cmd

import (
	"errors"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"gitee.com/lbzs/demo/hostdemo/api/conf"
	"gitee.com/lbzs/demo/hostdemo/api/pkg"
	"gitee.com/lbzs/demo/hostdemo/api/pkg/host/impl"
	"gitee.com/lbzs/demo/hostdemo/api/protocol"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/spf13/cobra"
)

var serviceCmd = &cobra.Command{
	Use:   "start",
	Short: "Demo后端API服务",
	Long:  "Demo后端API服务",
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := loadGlobalConfig(confType); err != nil {
			return err
		}

		if err := loadGlobalLogger(); err != nil {
			return err
		}

		if err := impl.Service.Config(); err != nil {
			return err
		}

		pkg.Host = impl.Service

		ch := make(chan os.Signal, 1)
		signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT, syscall.SIGHUP, syscall.SIGQUIT)

		svr, err := newService(conf.C())
		if err != nil {
			return err
		}
		go svr.waitSign(ch)
		if err := svr.start(); err != nil {
			if !strings.Contains(err.Error(), "http: Server closed") {
				return err
			}
		}
		return nil
	},
}

type service struct {
	log  logger.Logger
	http *protocol.HTTPService
}

func (s *service) start() error {
	return s.http.Start()

}
func (s *service) waitSign(sign chan os.Signal) {
	for sg := range sign {
		switch v := sg.(type) {
		default:
			s.log.Infof("recevice signal %v, start graceful shutdown", v.String())
			if err := s.http.Stop(); err != nil {
				s.log.Errorf("gracefu shutdown error: %s, force exit", err)

			}
			s.log.Infof("service stop complete")
			return
		}
	}
}
func newService(cnf *conf.Config) (*service, error) {
	http := protocol.NewHTTPService()
	svr := &service{
		http: http,
		log:  zap.L().Named("CLI"),
	}
	return svr, nil
}
func loadGlobalConfig(configType string) error {
	switch configType {
	case "file":
		err := conf.LoadConfigFromToml(confFile)
		if err != nil {
			return err
		}
	case "env":
		err := conf.LoadConfigFromEnv()
		if err != nil {
			return err
		}
	case "etcd":
		return errors.New("not implemented")
	default:
		return errors.New("unknown config type")
	}
	return nil
}

func loadGlobalLogger() error {
	var (
		logInitMsg string
		level      zap.Level
	)
	lc := conf.C().Log
	lv, err := zap.NewLevel(lc.Level)
	if err != nil {
		logInitMsg = fmt.Sprintf("%s, use default level INFO", err)
		level = zap.InfoLevel
	} else {
		level = lv
		logInitMsg = fmt.Sprintf("log level: %s", lv)
	}
	zapConfig := zap.DefaultConfig()
	zapConfig.Level = level
	zapConfig.Files.RotateOnStartup = false
	switch lc.To {
	case conf.ToStdout:
		zapConfig.ToStderr = true
		zapConfig.ToFiles = false
	case conf.ToFile:
		zapConfig.Files.Name = "api.log"
		zapConfig.Files.Path = lc.PathDir
	}
	switch lc.Format {
	case conf.JSONFormat:
		zapConfig.JSON = true
	}
	if err := zap.Configure(zapConfig); err != nil {
		return err
	}
	zap.L().Named("INIT").Info(logInitMsg)
	return nil
}

func init() {
	RootCmd.AddCommand(serviceCmd)
}

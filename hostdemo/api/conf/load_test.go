package conf

import (
	"fmt"
	"testing"

	"github.com/BurntSushi/toml"
)

func TestLoad(t *testing.T) {
	cfg := newConfig()
	filepath := "D:\\gomoduleproject\\hostdemo\\api\\etc\\demo-api.toml"
	if _, err := toml.DecodeFile(filepath, cfg); err != nil {
		fmt.Println(err, cfg)
	}
	fmt.Println(cfg)
}

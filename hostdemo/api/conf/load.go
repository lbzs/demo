package conf

import (
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

var (
	global *Config
)

func C() *Config {
	if global == nil {
		panic("Load config first")
	}
	return global
}

func LoadConfigFromToml(filePath string) error {
	cfg := newConfig()
	if _, err := toml.DecodeFile(filePath, cfg); err != nil {
		// fmt.Println("open file")
		return err
	}
	global = cfg
	// fmt.Println(global)
	return nil
}

func LoadConfigFromEnv() error {
	cfg := newConfig()
	if err := env.Parse(cfg); err != nil {
		return err
	}
	global = cfg
	return nil
}
